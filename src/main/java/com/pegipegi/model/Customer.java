/**
 * 
 */
package com.pegipegi.model;

import java.io.Serializable;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8614373373473235432L;
	
	long custId;
	String name;
	long age;
  private Address address;
  
	public long getCustId()
	{
		return custId;
	}
	public void setCustId(long custId)
	{
		this.custId = custId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public long getAge()
	{
		return age;
	}
	public void setAge(long age)
	{
		this.age = age;
	}
	public Address getAddress()
	{
		return address;
	}
	public void setAddress(Address address)
	{
		this.address = address;
	}
  
}
