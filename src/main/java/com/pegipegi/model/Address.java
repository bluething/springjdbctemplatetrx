/**
 * 
 */
package com.pegipegi.model;

import java.io.Serializable;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 20304203315426050L;
	
	private int id;
  private String address;
  private String country;
  
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getAddress()
	{
		return address;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
  
  
}
