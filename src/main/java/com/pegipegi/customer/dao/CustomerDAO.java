/**
 * 
 */
package com.pegipegi.customer.dao;

import com.pegipegi.model.Customer;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public interface CustomerDAO {
	public void insert(Customer customer);
}
