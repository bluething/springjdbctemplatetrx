/**
 * 
 */
package com.pegipegi.customer.dao.impl;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.model.Customer;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class CustomerDAOImpl implements CustomerDAO {
	
	private DataSource datasource;

	/* (non-Javadoc)
	 * @see com.pegipegi.customer.dao.CustomerDAO#insert(com.pegipegi.model.Customer)
	 */
	@Override
	@Transactional
	public void insert(Customer customer)
	{
		String sql = "INSERT INTO CUSTOMER (CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		String sql2 = "INSERT INTO ADDRESS (ADDRESS_ID, ADDRESS, COUNTRY) VALUES (?, ?, ?)";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		
		Object[] args = new Object[] {customer.getCustId(), customer.getName(), customer.getAge()};
		jdbcTemplate.update(sql, args);
		System.out.println("Inserted into Customer Table Successfully");
		
		jdbcTemplate.update(sql2, new Object[] { customer.getCustId(),
        customer.getAddress().getAddress(),
        customer.getAddress().getCountry() });
		System.out.println("Inserted into Address Table Successfully");
  }
		

	public void setDatasource(DataSource datasource)
	{
		this.datasource = datasource;
	}

}
