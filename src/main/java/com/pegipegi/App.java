package com.pegipegi;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pegipegi.customer.dao.CustomerDAO;
import com.pegipegi.model.Address;
import com.pegipegi.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{
		System.out.println( "Hello World!" );
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		CustomerDAO customerDAO = (CustomerDAO) context.getBean("customerDAO");
		Address address = new Address();
		address.setId(2);
		address.setCountry("Indonesia");

		Customer customer = new Customer();
		customer.setCustId(10);
		customer.setAge(31);
		customer.setName("Habib");
		customer.setAddress(address);
		customerDAO.insert(customer);
		System.out.println(customer);
		
		context.close();
	}
}
